package com.gitlab.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootGitlabApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootGitlabApplication.class, args);
	}

}
