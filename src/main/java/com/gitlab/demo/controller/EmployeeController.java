package com.gitlab.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gitlab.demo.dto.Employee;
import com.gitlab.demo.service.EmployeeService;

@RestController
@RequestMapping("/v1/api")
public class EmployeeController {

	@Autowired
	private EmployeeService employeeService;

	@GetMapping("/listOfEmployees")
	public List<Employee> findAll() {
		return employeeService.findAllEmployees();
	}

}
