package com.gitlab.demo.service;

import java.util.List;

import com.gitlab.demo.dto.Employee;

public interface EmployeeService {

	public List<Employee> findAllEmployees();

}
